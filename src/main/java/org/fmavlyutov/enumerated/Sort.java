package org.fmavlyutov.enumerated;

import org.fmavlyutov.comparator.CreatedComparator;
import org.fmavlyutov.comparator.NameComparator;
import org.fmavlyutov.comparator.StatusComparator;
import org.fmavlyutov.model.AbstractModel;

import java.util.Comparator;

public enum Sort {

    BY_NAME("Sort by name", NameComparator.INSTANCE),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE);

    private final String displayName;

    private final Comparator<?> comparator;

    Sort(String displayName, Comparator<?> comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    public static Sort toSort(final String value) {
        if (value == null || value.isEmpty()) {
            return null;
        }
        for (final Sort sort : values()) {
            if (sort.name().equals(value)) {
                return sort;
            }
        }
        return null;
    }

    public String getDisplayName() {
        return displayName;
    }

    @SuppressWarnings("unchecked")
    public <T> Comparator<T> getComparator() {
        return (Comparator<T>) comparator;
    }

}
