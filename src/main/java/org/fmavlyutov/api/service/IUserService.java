package org.fmavlyutov.api.service;

import org.fmavlyutov.enumerated.Role;
import org.fmavlyutov.model.User;

public interface IUserService extends IService<User> {

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User removeById(String id);

    User findOneByLogin(String login);

    User findOneByEmail(String email);

    boolean isLoginExists(String login);

    boolean isEmailExists(String email);

    User removeByLogin(String login);

    User removeByEmail(String email);

    User setPassword(String id, String password);

    User updateUser(String id, String firstName, String lastName, String middleName);

    void lockUserByLogin(String login);

    void unlockUserByLogin(String login);

}
