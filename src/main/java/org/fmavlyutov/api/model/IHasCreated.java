package org.fmavlyutov.api.model;

import java.util.Date;

public interface IHasCreated {

    Date getCreated();

    void setCreated(Date date);

}
