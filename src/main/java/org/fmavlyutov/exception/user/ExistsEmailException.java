package org.fmavlyutov.exception.user;

public final class ExistsEmailException extends AbstractUserException {

    public ExistsEmailException() {
        super("Email already exists!");
    }
}
