package org.fmavlyutov.exception.user;

import org.fmavlyutov.exception.field.AbstractFieldException;

public final class EmptyUserIdException extends AbstractFieldException {

    public EmptyUserIdException() {
        super("User id can not be empty!");
    }

    public EmptyUserIdException(String message) {
        super(message);
    }

}
