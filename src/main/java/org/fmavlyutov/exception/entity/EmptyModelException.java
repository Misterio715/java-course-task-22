package org.fmavlyutov.exception.entity;

public final class EmptyModelException extends AbstractEntityException {

    public EmptyModelException() {
        super("This model can not have empty values!");
    }

}
