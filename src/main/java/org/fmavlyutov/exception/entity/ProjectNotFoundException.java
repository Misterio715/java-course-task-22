package org.fmavlyutov.exception.entity;

public final class ProjectNotFoundException extends AbstractEntityException {

    public ProjectNotFoundException() {
        super("Project not found!");
    }
}
