package org.fmavlyutov.exception.entity;

public final class TaskNotFoundException extends AbstractEntityException {

    public TaskNotFoundException() {
        super("Task not found!");
    }
}
