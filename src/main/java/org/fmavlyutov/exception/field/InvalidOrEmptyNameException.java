package org.fmavlyutov.exception.field;

public final class InvalidOrEmptyNameException extends AbstractFieldException {

    public InvalidOrEmptyNameException() {
        super("Invalid or empty name!");
    }

}
