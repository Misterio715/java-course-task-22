package org.fmavlyutov.service;

import org.fmavlyutov.api.repository.IProjectRepository;
import org.fmavlyutov.api.repository.ITaskRepository;
import org.fmavlyutov.api.repository.IUserRepository;
import org.fmavlyutov.api.service.IUserService;
import org.fmavlyutov.enumerated.Role;
import org.fmavlyutov.exception.entity.UserNotFoundException;
import org.fmavlyutov.exception.field.InvalidOrEmptyEmailException;
import org.fmavlyutov.exception.field.InvalidOrEmptyIdException;
import org.fmavlyutov.exception.field.InvalidOrEmptyLoginException;
import org.fmavlyutov.exception.field.InvalidOrEmptyPasswordException;
import org.fmavlyutov.exception.user.EmptyRoleException;
import org.fmavlyutov.exception.user.ExistsEmailException;
import org.fmavlyutov.exception.user.ExistsLoginException;
import org.fmavlyutov.model.User;
import org.fmavlyutov.util.HashUtil;

public final class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public UserService(IUserRepository repository, IProjectRepository projectRepository, ITaskRepository taskRepository) {
        super(repository);
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public User create(final String login, final String password) {
        if (login == null || login.isEmpty()) {
            throw new InvalidOrEmptyLoginException();
        }
        if (isLoginExists(login)) {
            throw new ExistsLoginException();
        }
        if (password == null || password.isEmpty()) {
            throw new InvalidOrEmptyPasswordException();
        }
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        return repository.add(user);
    }

    @Override
    public User create(final String login, final String password, final String email) {
        if (login == null || login.isEmpty()) {
            throw new InvalidOrEmptyLoginException();
        }
        if (isLoginExists(login)) {
            throw new ExistsLoginException();
        }
        if (password == null || password.isEmpty()) {
            throw new InvalidOrEmptyPasswordException();
        }
        if (isEmailExists(email)) {
            throw new ExistsEmailException();
        }
        final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @Override
    public User create(final String login, final String password, final Role role) {
        if (login == null || login.isEmpty()) {
            throw new InvalidOrEmptyLoginException();
        }
        if (isLoginExists(login)) {
            throw new ExistsLoginException();
        }
        if (role == null) {
            throw new EmptyRoleException();
        }
        final User user = create(login, password);
        if (role != null) {
            user.setRole(role);
        }
        return user;
    }

    @Override
    public User findOneByLogin(final String login) {
        if (login == null || login.isEmpty()) {
            throw new InvalidOrEmptyLoginException();
        }
        final User user = repository.findOneByLogin(login);
        if (user == null) {
            return null;
        }
        return user;
    }

    @Override
    public User findOneByEmail(final String email) {
        if (email == null || email.isEmpty()) {
            throw new InvalidOrEmptyEmailException();
        }
        final User user = repository.findOneByEmail(email);
        if (user == null) {
            return null;
        }
        return user;
    }

    @Override
    public User remove(final User model) {
        if (model == null) {
            return null;
        }
        final User user = super.remove(model);
        if (user == null) {
            return null;
        }
        final String userId = user.getId();
        taskRepository.clear(userId);
        projectRepository.clear(userId);
        return user;
    }

    @Override
    public User removeByLogin(final String login) {
        if (login == null || login.isEmpty()) {
            throw new InvalidOrEmptyLoginException();
        }
        final User user = repository.findOneByLogin(login);
        return remove(user);
    }

    @Override
    public User removeByEmail(final String email) {
        if (email == null || email.isEmpty()) {
            throw new InvalidOrEmptyEmailException();
        }
        final User user = repository.findOneByEmail(email);
        return remove(user);
    }

    @Override
    public User setPassword(final String id, final String password) {
        if (id == null || id.isEmpty()) {
            throw new InvalidOrEmptyIdException();
        }
        if (password == null || password.isEmpty()) {
            throw new InvalidOrEmptyPasswordException();
        }
        final User user = findOneById(id);
        if (user == null) {
            throw new UserNotFoundException();
        }
        user.setPasswordHash(HashUtil.salt(password));
        return user;
    }

    @Override
    public User updateUser(final String id, final String firstName, final String lastName, final String middleName) {
        if (id == null || id.isEmpty()) {
            throw new InvalidOrEmptyIdException();
        }
        final User user = findOneById(id);
        if (user == null) {
            throw new UserNotFoundException();
        }
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    @Override
    public boolean isLoginExists(final String login) {
        if (login == null || login.isEmpty()) {
            return false;
        }
        return repository.isLoginExists(login);
    }

    @Override
    public boolean isEmailExists(final String email) {
        if (email == null || email.isEmpty()) {
            return false;
        }
        return repository.isEmailExists(email);
    }

    @Override
    public void lockUserByLogin(final String login) {
        if (login == null || login.isEmpty()) {
            throw new InvalidOrEmptyLoginException();
        }
        final User user = findOneByLogin(login);
        if (user == null) {
            throw new UserNotFoundException();
        }
        user.setLocked(true);
    }

    @Override
    public void unlockUserByLogin(final String login) {
        if (login == null || login.isEmpty()) {
            throw new InvalidOrEmptyLoginException();
        }
        final User user = findOneByLogin(login);
        if (user == null) {
            throw new UserNotFoundException();
        }
        user.setLocked(false);
    }

}
