package org.fmavlyutov.service;

import org.fmavlyutov.api.repository.IProjectRepository;
import org.fmavlyutov.api.service.IProjectService;
import org.fmavlyutov.enumerated.Status;
import org.fmavlyutov.exception.entity.ProjectNotFoundException;
import org.fmavlyutov.exception.field.InvalidOrEmptyIdException;
import org.fmavlyutov.exception.field.InvalidOrEmptyIndexException;
import org.fmavlyutov.exception.field.InvalidOrEmptyNameException;
import org.fmavlyutov.exception.user.EmptyUserIdException;
import org.fmavlyutov.model.Project;

public final class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(IProjectRepository repository) {
        super(repository);
    }

    @Override
    public Project create(final String userId, final String name, final String description) {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyUserIdException();
        }
        if (name == null || name.isEmpty()) {
            throw new InvalidOrEmptyNameException();
        }
        final Project project = new Project();
        project.setName(name);
        if (description != null) {
            project.setDescription(description);
        }
        return add(userId, project);
    }

    @Override
    public Project updateById(final String userId, final String id, final String name, final String description) {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyUserIdException();
        }
        if (id == null || id.isEmpty()) {
            throw new InvalidOrEmptyIdException();
        }
        if (name == null | name.isEmpty()) {
            throw new InvalidOrEmptyNameException();
        }
        final Project project = repository.findOneById(userId, id);
        if (project == null) {
            throw new ProjectNotFoundException();
        }
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateByIndex(final String userId, final Integer index, final String name, final String description) {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyUserIdException();
        }
        if (index == null || index < 0 || index >= repository.getSize()) {
            throw new InvalidOrEmptyIndexException();
        }
        if (name == null | name.isEmpty()) {
            throw new InvalidOrEmptyNameException();
        }
        final Project project = repository.findOneByIndex(userId, index);
        if (project == null) {
            throw new ProjectNotFoundException();
        }
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project changeStatusById(final String userId, final String id, final Status status) {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyUserIdException();
        }
        if (id == null || id.isEmpty()) {
            throw new InvalidOrEmptyIdException();
        }
        final Project project = repository.findOneById(userId, id);
        if (project == null) {
            throw new ProjectNotFoundException();
        }
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeStatusByIndex(final String userId, final Integer index, final Status status) {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyUserIdException();
        }
        if (index == null || index < 0 || index >= repository.getSize()) {
            throw new InvalidOrEmptyIndexException();
        }
        final Project project = repository.findOneByIndex(userId, index);
        if (project == null) {
            throw new ProjectNotFoundException();
        }
        project.setStatus(status);
        return project;
    }

}
