package org.fmavlyutov.command.user;

import org.fmavlyutov.enumerated.Role;
import org.fmavlyutov.util.TerminalUtil;

public final class UserRemoveByLoginCommand extends AbstractUserCommand {

    @Override
    public Role[] getRoles() {
        return new Role[] { Role.ADMIN };
    }

    @Override
    public String getDescription() {
        return "remove user by login";
    }

    @Override
    public String getName() {
        return "user-remove-by-login";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE USER BY LOGIN]");
        System.out.println("Enter login: ");
        final String login = TerminalUtil.nextLine();
        getUserService().removeByLogin(login);
    }

}
