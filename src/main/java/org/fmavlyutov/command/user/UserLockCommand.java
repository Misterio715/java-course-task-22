package org.fmavlyutov.command.user;

import org.fmavlyutov.enumerated.Role;
import org.fmavlyutov.model.User;
import org.fmavlyutov.util.TerminalUtil;

public final class UserLockCommand extends AbstractUserCommand {

    @Override
    public Role[] getRoles() {
        return new Role[] { Role.ADMIN };
    }

    @Override
    public String getDescription() {
        return "lock user";
    }

    @Override
    public String getName() {
        return "user-lock";
    }

    @Override
    public void execute() {
        System.out.println("[LOCK USER]");
        System.out.println("Enter login: ");
        final String login = TerminalUtil.nextLine();
        getUserService().lockUserByLogin(login);
    }

}
