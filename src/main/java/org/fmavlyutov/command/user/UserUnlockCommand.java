package org.fmavlyutov.command.user;

import org.fmavlyutov.enumerated.Role;
import org.fmavlyutov.util.TerminalUtil;

public final class UserUnlockCommand extends AbstractUserCommand {

    @Override
    public Role[] getRoles() {
        return new Role[] { Role.ADMIN };
    }

    @Override
    public String getDescription() {
        return "unlock user";
    }

    @Override
    public String getName() {
        return "user-unlock";
    }

    @Override
    public void execute() {
        System.out.println("[UNLOCK USER]");
        System.out.println("Enter login: ");
        final String login = TerminalUtil.nextLine();
        getUserService().unlockUserByLogin(login);
    }

}
