package org.fmavlyutov.command.user;

import org.fmavlyutov.enumerated.Role;
import org.fmavlyutov.util.TerminalUtil;

public final class UserRemoveByEmailCommand extends AbstractUserCommand {

    @Override
    public Role[] getRoles() {
        return new Role[] { Role.ADMIN };
    }

    @Override
    public String getDescription() {
        return "remove user by email";
    }

    @Override
    public String getName() {
        return "user-remove-by-email";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE USER BY EMAIL]");
        System.out.println("Enter email: ");
        final String email = TerminalUtil.nextLine();
        getUserService().removeByEmail(email);
    }

}
