package org.fmavlyutov.command.user;

import org.fmavlyutov.enumerated.Role;
import org.fmavlyutov.util.TerminalUtil;

public final class UserLoginCommand extends AbstractUserCommand {

    @Override
    public Role[] getRoles() {
        return null;
    }

    @Override
    public String getDescription() {
        return "login user";
    }

    @Override
    public String getName() {
        return "login";
    }

    @Override
    public void execute() {
        System.out.println("[LOGIN USER]");
        System.out.println("Enter login:");
        final String login = TerminalUtil.nextLine();
        System.out.println("Enter password");
        final String password = TerminalUtil.nextLine();
        getAuthService().login(login, password);
    }

}
