package org.fmavlyutov.command.project;

import org.fmavlyutov.api.service.IProjectService;
import org.fmavlyutov.api.service.IProjectTaskService;
import org.fmavlyutov.command.AbstractCommand;
import org.fmavlyutov.enumerated.Role;
import org.fmavlyutov.model.Project;

public abstract class AbstractProjectCommand extends AbstractCommand {

    protected IProjectService getProjectService() {
        return getServiceLocator().getProjectService();
    }

    protected IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    public String getArgument() {
        return null;
    }

    protected void showProject(final Project project) {
        if (project == null) {
            return;
        }
        System.out.println(project);
    }

}
