package org.fmavlyutov.command.project;

import org.fmavlyutov.enumerated.Status;
import org.fmavlyutov.model.Project;
import org.fmavlyutov.util.TerminalUtil;

import java.util.Arrays;

public final class ProjectChangeStatusByIdCommand extends AbstractProjectCommand {

    @Override
    public String getDescription() {
        return "change project status by id";
    }

    @Override
    public String getName() {
        return "project-change-status-by-id";
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE PROJECT STATUS]");
        System.out.println("Enter id:");
        String id = TerminalUtil.nextLine();
        System.out.println("Enter status:");
        System.out.println(Arrays.toString(Status.values()));
        final String status = TerminalUtil.nextLine();
        final Project project = getProjectService().changeStatusById(getUserId(), id, Status.toStatus(status));
        showProject(project);
    }

}
